provider "azurerm" {
	features {}
}

resource "azurerm_resource_group" "rg" {
        name = "wtw_salt_resource_group"
        location = "eastus"
}

# Create virtual network
resource "azurerm_virtual_network" "virt_net" {
    name                = "wtw_virt_net"
    address_space       = ["10.0.1.0/25"]
    location            = "eastus"
    resource_group_name = azurerm_resource_group.rg.name

}

# Create subnet
resource "azurerm_subnet" "wtw_subnet" {
    name                 = "wtw_subnet"
    resource_group_name  = azurerm_resource_group.rg.name
    virtual_network_name = azurerm_virtual_network.virt_net.name
    address_prefix       = "10.0.1.0/25"
}

# Create public IPs
resource "azurerm_public_ip" "wtw_pub_ip_master" {
    name                         = "wtw_pub_ip_master"
    location                     = "eastus"
    resource_group_name          = azurerm_resource_group.rg.name
    allocation_method            = "Dynamic"

}

# Create public IPs
resource "azurerm_public_ip" "wtw_pub_ip_minion" {
    name                         = "wtw_pub_ip_minion"
    location                     = "eastus"
    resource_group_name          = azurerm_resource_group.rg.name
    allocation_method            = "Dynamic"

}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "wtw_nsg" {
    name                = "wtw_nsg"
    location            = "eastus"
    resource_group_name = azurerm_resource_group.rg.name
    
    security_rule {
        name                       = "ssh"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "http80"
        priority                   = 1002
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "*"
        source_port_range          = "*"
        destination_port_range     = "80"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "http443"
        priority                   = 1003
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "*"
        source_port_range          = "*"
        destination_port_range     = "443"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "http3200"
        priority                   = 1004
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "*"
        source_port_range          = "*"
        destination_port_range     = "3200"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

}

# Create network interface
resource "azurerm_network_interface" "wtw_nic_master" {
    name                      = "wtw_nic_master"
    location                  = "eastus"
    resource_group_name       = azurerm_resource_group.rg.name

    ip_configuration {
        name                          = "wtw_nic_master_conf"
        subnet_id                     = azurerm_subnet.wtw_subnet.id
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = azurerm_public_ip.wtw_pub_ip_master.id
    }

}

# Create network interface
resource "azurerm_network_interface" "wtw_nic_minion" {
name                      = "wtw_nic_minion"
location                  = "eastus"
resource_group_name       = azurerm_resource_group.rg.name
ip_configuration {
	name                          = "wtw_nic_minion_conf"
	subnet_id                     = azurerm_subnet.wtw_subnet.id
	private_ip_address_allocation = "Dynamic"
	public_ip_address_id          = azurerm_public_ip.wtw_pub_ip_minion.id
    }
}

resource "azurerm_network_interface_security_group_association" "wtw_nsg_asoc_master" {
  network_interface_id      = azurerm_network_interface.wtw_nic_master.id
  network_security_group_id = azurerm_network_security_group.wtw_nsg.id
}

resource "azurerm_network_interface_security_group_association" "wtw_nsg_asoc_minion" {
  network_interface_id      = azurerm_network_interface.wtw_nic_minion.id
  network_security_group_id = azurerm_network_security_group.wtw_nsg.id
}

# Generate random text for a unique storage account name
resource "random_id" "randomId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = azurerm_resource_group.rg.name
    }
    
    byte_length = 8
}

# Create storage account for boot diagnostics
resource "azurerm_storage_account" "wtw_storage" {
    name                        = "diag${random_id.randomId.hex}"
    resource_group_name         = azurerm_resource_group.rg.name
    location                    = "eastus"
    account_tier                = "Standard"
    account_replication_type    = "LRS"

}

# Create salt master vm
resource "azurerm_virtual_machine" "wtw_salt_master" {
    name                  = "wtw_salt_master"
    location              = "eastus"
    resource_group_name   = azurerm_resource_group.rg.name
    network_interface_ids = [azurerm_network_interface.wtw_nic_master.id]
    vm_size               = "Standard_DS1_v2"

    storage_os_disk {
        name              = "wtw_master_disk"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    }

    storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "16.04.0-LTS"
        version   = "latest"
    }

    os_profile {
        computer_name  = "wtw-master"
        admin_username = "wtw_usr"
    }

    os_profile_linux_config {
        disable_password_authentication = true
        ssh_keys {
            path     = "/home/wtw_usr/.ssh/authorized_keys"
            key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDWdmoS/u2K+78KK66WIhpyptc5UMWhz9X7mj/BjmNDZK2mfqUBeE7ywX41YTozwMBcn0BF7+OMn8DurisHQAhptepTMEsz+YDIyYTTi+x1iJvJgjJ3UG+3YPA7znUBDkSurO43yPQAM/3MdxYsFKUpghEkk350KL+lgE5sUx3yjCnlD909l/xXxjz3HciymaIVmDfw49oMfdp7xQ5xGl9EZMfwtW9UilDbUIuyObw7DJXbyiXVVfg1tz19XKVArrd77Je2pyU+w6wIEDOqSRWP+BVycd3ONkZdSvnsdnXg43Ty27o+Uy+FHHHd2TrnVZB8OVOSgod6dMMExCiol9rPQoYhzzDaqmxgzdp1ORlsrENJzxANFOnMyed1JtW5FaZhEZItyHxAXvBF1QLTM0qpqHpqHD9fWOV7gNu4iNepbKkFA6G76ywZC8WhMeMnrYYM3bvtdNpuLiXhvqYsMtHiy4bn66lPmQzEiPvKwZl/RC7VybfBUXPH205jdKemceV7DSAxOiquOc6FJNZbMTFuf8fE4pZ6V0YJacu3YCCQIo2KJ9ckksGeYbg90xXStM9G0zOtUoAd8oNfNxu6nYiDBU4x0n3QFJupeverf64HEkgUsZeOQn27oYMunyywf6h+Fn8ieHi2Cc5uCH9INKXA001BJMC6Ua9B8epEcqgnew=="
        }
    }

    boot_diagnostics {
        enabled = "true"
        storage_uri = azurerm_storage_account.wtw_storage.primary_blob_endpoint
    }

}

# Create salt minion vm
resource "azurerm_virtual_machine" "wtw_salt_masterlt_minion" {
    name                  = "wtw_salt_minion"
    location              = "eastus"
    resource_group_name   = azurerm_resource_group.rg.name
    network_interface_ids = [azurerm_network_interface.wtw_nic_minion.id]
    vm_size               = "Standard_DS1_v2"

    storage_os_disk {
        name              = "wtw_minion_disk"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    }

    storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "16.04.0-LTS"
        version   = "latest"
    }

    os_profile {
        computer_name  = "wtw-minion"
        admin_username = "wtw_usr"
    }

    os_profile_linux_config {
        disable_password_authentication = true
        ssh_keys {
            path     = "/home/wtw_usr/.ssh/authorized_keys"
            key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDWdmoS/u2K+78KK66WIhpyptc5UMWhz9X7mj/BjmNDZK2mfqUBeE7ywX41YTozwMBcn0BF7+OMn8DurisHQAhptepTMEsz+YDIyYTTi+x1iJvJgjJ3UG+3YPA7znUBDkSurO43yPQAM/3MdxYsFKUpghEkk350KL+lgE5sUx3yjCnlD909l/xXxjz3HciymaIVmDfw49oMfdp7xQ5xGl9EZMfwtW9UilDbUIuyObw7DJXbyiXVVfg1tz19XKVArrd77Je2pyU+w6wIEDOqSRWP+BVycd3ONkZdSvnsdnXg43Ty27o+Uy+FHHHd2TrnVZB8OVOSgod6dMMExCiol9rPQoYhzzDaqmxgzdp1ORlsrENJzxANFOnMyed1JtW5FaZhEZItyHxAXvBF1QLTM0qpqHpqHD9fWOV7gNu4iNepbKkFA6G76ywZC8WhMeMnrYYM3bvtdNpuLiXhvqYsMtHiy4bn66lPmQzEiPvKwZl/RC7VybfBUXPH205jdKemceV7DSAxOiquOc6FJNZbMTFuf8fE4pZ6V0YJacu3YCCQIo2KJ9ckksGeYbg90xXStM9G0zOtUoAd8oNfNxu6nYiDBU4x0n3QFJupeverf64HEkgUsZeOQn27oYMunyywf6h+Fn8ieHi2Cc5uCH9INKXA001BJMC6Ua9B8epEcqgnew=="
        }
    }

    boot_diagnostics {
        enabled = "true"
        storage_uri = azurerm_storage_account.wtw_storage.primary_blob_endpoint
    }

}
