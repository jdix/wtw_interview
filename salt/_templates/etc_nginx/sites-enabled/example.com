server {
        listen 3200;
        root /var/www/html;
        server_name example.com;
        location / {
                proxy_pass http://localhost:3400;
        }
}

server {
  server_name _;
  root /var/www/html;
  error_page 404 /404.html;
  listen 3200 default_server deferred;
  listen 80 default_server deferred;
  location / {
    return 404;  
  }
  location  /404.html {
    internal;
  }
}
