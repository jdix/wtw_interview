install_nginx:
  pkg.installed:
    - pkgs:
      - nginx

remove_default_site:
  cmd.run:
    - name: 'sudo rm /etc/nginx/sites-enabled/default'

apply_custom_nginx_conf:
  file.recurse:
    - name: /etc/nginx/
    - source: salt://_templates/etc_nginx/

apply_custom_nginx_sites:
  file.recurse:
    - name: /var/www/
    - source: salt://_templates/var_www/

enable_nginx:
  service.running:
    - name: nginx
    - enable: True
    - restart: True

hard_restart_nginx:
  cmd.run:
    - name: 'sudo systemctl restart nginx'