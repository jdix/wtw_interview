## terraform deploy:

- set terraform azure environment variables for login in azure_config.sh
- run azure_config.sh
- change ssh key for vm in simple_azure.tf

```
terraform init
terraform apply
```

## on master:
```
curl -L https://bootstrap.saltstack.com -o install_salt.sh
sudo sh install_salt.sh -P -M
sudo mkdir -p /srv/{salt,pillar,formulas}
echo "
file_roots:
  base:
    - /srv/salt
    - /srv/formulas" | sudo tee /etc/salt/master
echo "
pillar_roots:
  base:
    - /srv/pillar" | sudo tee -a /etc/salt/master
sudo systemctl restart salt-master
echo "master: 127.0.0.1" | sudo tee -a /etc/salt/minion
sudo systemctl restart salt-minion
sudo salt-key -a wtw-master.internal.cloudapp.net
```

## on minion:
```
curl -L https://bootstrap.saltstack.com -o install_salt.sh
sudo sh install_salt.sh -P
echo "master: wtw-master.internal.cloudapp.net" | sudo tee -a /etc/salt/minion
sudo systemctl restart salt-minion
```
## on master:
```
sudo salt-key -a wtw-minion # .internal.cloudapp.net # may need fqdn
```

- at this point, move the git project with the salt files which go to /srv/salt/x
- copy salt dir to master /srv/salt/ "sudo cp -R ~/salt/* /srv/salt/"

```
# sudo salt 'wtw-minion.internal.cloudapp.net' state.apply nginx_conf
sudo salt 'wtw-minion' state.apply nginx_conf
```